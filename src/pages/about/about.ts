import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  profs: { name: string; spec: string; img: string; }[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profs=[
      {name:'Abdullah Al-Anqari' , spec:'Chairman',img:'prof1'},
      {name:'Bandar Al Saud' , spec:'Deputy Chairman',img:'prof2'},
      {name:'Suleiman Alqazlan' , spec:'Secertary',img:'prof4'},
      {name:'Mohammed Al Rabee' , spec:'Member',img:'prof3'}
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

}
