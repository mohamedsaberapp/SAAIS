import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialistprofilePage } from './specialistprofile';

@NgModule({
  declarations: [
    SpecialistprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialistprofilePage),
  ],
})
export class SpecialistprofilePageModule {}
