import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemberareaPage } from './memberarea';

@NgModule({
  declarations: [
    MemberareaPage,
  ],
  imports: [
    IonicPageModule.forChild(MemberareaPage),
  ],
})
export class MemberareaPageModule {}
