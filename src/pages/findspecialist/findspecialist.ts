import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchPage } from '../search/search';
import { SpecialistprofilePage } from '../specialistprofile/specialistprofile';

/**
 * Generated class for the FindspecialistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-findspecialist',
  templateUrl: 'findspecialist.html',
})
export class FindspecialistPage {
findSpec:string="byName";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FindspecialistPage');
  }
  goSearch(){
    this.navCtrl.push(SearchPage);
  }
  viewSpec(){
    this.navCtrl.push(SpecialistprofilePage);
  }

}
