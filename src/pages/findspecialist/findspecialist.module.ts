import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindspecialistPage } from './findspecialist';

@NgModule({
  declarations: [
    FindspecialistPage,
  ],
  imports: [
    IonicPageModule.forChild(FindspecialistPage),
  ],
})
export class FindspecialistPageModule {}
