import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  base64Image: string;
  myPic: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private toast: ToastController, private nativeStorage: NativeStorage) {
    this.nativeStorage.getItem('myPic')
      .then(
        data => {
          console.log(data);
          this.myPic = data;
        },
        error => console.error(error)
      );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  updatePic() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 500,
      targetHeight: 500,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      let suc = this.toast.create({
        message: "Successfully changed !",
        duration: 1000
      })
      suc.present();
      //Storing Pic in native storage
      this.nativeStorage.setItem('myPic', this.base64Image)
        .then(
          () => {
            console.log('Stored item!');
            let sucStored = this.toast.create({
              message: "Successfully stored !",
              duration: 2000
            })
            sucStored.present();
            this.nativeStorage.getItem('myPic')
              .then(
                data => {
                  console.log(data);
                  this.myPic = data;
                },
                error => console.error(error)
              );

          },
          error => {
            console.error('Error storing item', error);
            let errStored = this.toast.create({
              message: error,
              duration: 2000
            })
            errStored.present();
          }
        );


    }, (err) => {
      let toast = this.toast.create({
        message: err,
        duration: 3000
      })
      toast.present();
      // Handle error
    });
  }

}
