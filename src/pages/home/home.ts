import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsPage } from '../news/news';
import { DonationPage } from '../donation/donation';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { EducationPage } from '../education/education';
import { LoginPage } from '../login/login';
import { SearchPage } from '../search/search';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  goFind(){
    this.navCtrl.push(SearchPage);
  }
  goNews(){
    this.navCtrl.push(NewsPage);
  }
  goDonation(){
    this.navCtrl.push(DonationPage);
  }
  goAbout(){
    this.navCtrl.push(AboutPage);
  }
  goContact(){
    this.navCtrl.push(ContactPage);
  }
  goEdu(){
    this.navCtrl.push(EducationPage);
  }
  goLogin(){
    this.navCtrl.push(LoginPage);
  }

}
