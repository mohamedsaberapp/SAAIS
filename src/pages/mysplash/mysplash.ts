import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the MysplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysplash',
  templateUrl: 'mysplash.html',
})
export class MysplashPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    setTimeout(() => {
      
        this.navCtrl.setRoot(HomePage);
      
     }, 3000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MysplashPage');
  }

}
