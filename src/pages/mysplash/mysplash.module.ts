import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MysplashPage } from './mysplash';

@NgModule({
  declarations: [
    MysplashPage,
  ],
  imports: [
    IonicPageModule.forChild(MysplashPage),
  ],
})
export class MysplashPageModule {}
