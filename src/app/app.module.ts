import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AboutPage } from '../pages/about/about';
import { ArticlePage } from '../pages/article/article';
import { ContactPage } from '../pages/contact/contact';
import { DonationPage } from '../pages/donation/donation';
import { EducationPage } from '../pages/education/education';
import { FindspecialistPage } from '../pages/findspecialist/findspecialist';
import { LoginPage } from '../pages/login/login';
import { MemberareaPage } from '../pages/memberarea/memberarea';
import { NewsPage } from '../pages/news/news';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { SpecialistprofilePage } from '../pages/specialistprofile/specialistprofile';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { Camera } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';
import { MysplashPage } from '../pages/mysplash/mysplash';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AboutPage,
    ArticlePage,
    ContactPage,
    DonationPage,
    EducationPage,
    FindspecialistPage,
    LoginPage,
    MemberareaPage,
    NewsPage,
    ProfilePage,
    RegisterPage,
    SearchPage,
    SpecialistprofilePage,
    SettingsPage,
    MysplashPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AboutPage,
    ArticlePage,
    ContactPage,
    DonationPage,
    EducationPage,
    FindspecialistPage,
    LoginPage,
    MemberareaPage,
    NewsPage,
    ProfilePage,
    RegisterPage,
    SearchPage,
    SpecialistprofilePage,
    SettingsPage,
    MysplashPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
